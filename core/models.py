from django.db import models

# Create your models here.


class Image(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField()
    photo = models.ImageField(upload_to = 'images')
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.id)

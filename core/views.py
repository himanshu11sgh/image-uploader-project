from django.shortcuts import render
from django.views.generic import View
from django.contrib import messages

from core.forms import ImageForm
from core.models import Image

# Create your views here.


class Home(View):
    def get(self, request):
        form = ImageForm()
        imgs = Image.objects.all()

        context = {
            'form': form,
            'imgs': imgs
        }

        return render(request, 'core/home.html', context)


    def post(self, request):
        form = ImageForm(data=request.POST, files=request.FILES)
        imgs = Image.objects.all()
        if form.is_valid():
            form.save()
            messages.success(request, 'Your Image is successfully uploaded')
        form = ImageForm()
        context = {
            'form': form,
            'imgs': imgs
        }
        return render(request, 'core/home.html', context)
    